import connexion
import yaml
import time
import os
import logging
from logging import config
from connexion import NoContent
import requests
import datetime
import json
from pykafka import KafkaClient

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

hostname = f'{app_config["events"]["hostname"]}:{app_config["events"]["port"]}'

max_connection_retry = app_config["events"]["max_retries"]
current_retry_count = 0

while current_retry_count < max_connection_retry:
    try:
        logger.info(f'[Retry #{current_retry_count}] Connecting to Kafka...')

        client = KafkaClient(hosts=hostname)
        topic = client.topics[str.encode(app_config["events"]["topic"])]
        break

    except:
        logger.error(f'Connection to Kafka failed in #{current_retry_count}!')
        time.sleep(app_config["events"]["sleep"])
        current_retry_count += 1


def select_book_to_borrow(body):
    """ Adds a book to the users profile to borrow """
    # Implement Here

    logger.info("Received event 'Borrow Your Book' request with a unique id of %s" % body["book_id"])
    # client = KafkaClient(hosts='acit3855-setc-mahsa.eastus.cloudapp.azure.com:9092')
    # topic = client.topics[str.encode(app_config["events"]["topic"])]
    producer = topic.get_sync_producer()

    msg = {"type": "bb",
           "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
           "payload": body}

    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))
    logger.info("Returned event 'Borrow Your Book' response (Id: %s) with status 201" % (body["book_id"]))

    return NoContent, 201


def reserve_item_to_use(body):
    """ Adds a desk/computer to the users profile to reserve for future use """
    # Implement Here

    logger.info("Received event 'Reserve Your Item' request with a unique id of %s" % body["item_id"])
    # client = KafkaClient(hosts='acit3855-setc-mahsa.eastus.cloudapp.azure.com:9092')
    # topic = client.topics[str.encode(app_config["events"]["topic"])]
    producer = topic.get_sync_producer()
    msg = {"type": "ir", "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), "payload": body}
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))
    logger.info("Returned event 'Reserve Your Item' response (Id: %s) with status 201" % (body["item_id"]))

    return NoContent, 201


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yaml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    app.run(port=8080,host="0.0.0.0")
